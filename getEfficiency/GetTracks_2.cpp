/***************************************/
/*Usare questo script per ottenere n° di tracce e n° di cluster da un file .root che viene fuori dall'analisi. E' possibile tirare fuori l'efficienza di due DUT rispetto ad una di riferimento. */
/* Output su file << n tracce dut1 << n clust dut 1 << n tracce dut 2 << n clus dut 2 */
/*************************************/

/************************************/
// Use: 
// make 
/***********************************/             

#include <TApplication.h>
#include <TFile.h>
#include <TH2D.h>
#include <string>
#include <TProfile2D.h>
#include <list>
#include <fstream>

namespace data{
  
  std::string dut1_name = "plane151" ;
  std::string dut2_name = "plane130" ; 
  //std::string dut3_name = "plane132" ; 
  std::string dut4_name = "plane131" ; 
  
  //Load the list of the run numbers: numbers must be separated by a comma 
  std::list<int> run_number = {9416, 9417, 9418, 9419, 9420, 9421, 9422, 9423, 9424, 9425, 9426};
}

using namespace std ;

int main() {

  ofstream ofile("Efficency.dat") ; 
 
  for (int x : data::run_number) {
    string num = to_string(x) ;
    string filename1 = "./../run"+num+"/run00"+num+".root" ;
    
    string clust1_path = "AnalysisEfficiency/"+data::dut1_name+"/distanceTrackHit2D" ;
    string track1_path = "AnalysisEfficiency/"+data::dut1_name+"/pixelEfficiencyMap_trackPos_TProfile" ;
    string clust2_path = "AnalysisEfficiency/"+data::dut2_name+"/distanceTrackHit2D" ;
    string track2_path = "AnalysisEfficiency/"+data::dut2_name+"/pixelEfficiencyMap_trackPos_TProfile" ;
    //string clust3_path = "AnalysisEfficiency/"+data::dut3_name+"/distanceTrackHit2D" ;
    //string track3_path = "AnalysisEfficiency/"+data::dut3_name+"/pixelEfficiencyMap_trackPos_TProfile" ;
    string clust4_path = "AnalysisEfficiency/"+data::dut4_name+"/distanceTrackHit2D" ;
    string track4_path = "AnalysisEfficiency/"+data::dut4_name+"/pixelEfficiencyMap_trackPos_TProfile" ;
   

    /**************************************************************/
    //File for DUT1 with reference 1
    TFile *file_dut1 = new TFile(filename1.c_str());
    //Number of events 
    TH1F *Events1 = new TH1F() ;
    Events1 = (TH1F*)file_dut1->Get("Tracking4D/tracksPerEvent") ;
    //Cluster respect reference plane 1
    TH2D * Cluster1 = new TH2D();
    Cluster1 = (TH2D*)file_dut1->Get(clust1_path.c_str()); 
    //Cluster respect reference plane 1
    TProfile2D *Tracks1 = new TProfile2D() ;
    Tracks1 = (TProfile2D*)file_dut1->Get(track1_path.c_str()) ;
    
    /**************************************************************/
    //File for DUT2 with reference 1
    TFile *file_dut2 = new TFile(filename1.c_str());
    //Number of events 
    //TH1F *Events2 = new TH1F() ;
    //Events2 = (TH1F*)file_dut2->Get("Tracking4D/tracksPerEvent") ;
    //Cluster respect reference plane 1
    TH2D * Cluster2 = new TH2D();
    Cluster2 = (TH2D*)file_dut2->Get(clust2_path.c_str()); 
    //Cluster respect reference plane 1
    TProfile2D *Tracks2 = new TProfile2D() ;
    Tracks2 = (TProfile2D*)file_dut2->Get(track2_path.c_str()) ;
    
    /*************************************************************/

    /**************************************************************/
    //File for DUT3 with reference 1
    /*
    TFile *file_dut3 = new TFile(filename1.c_str());
    //Cluster respect reference plane 1
    TH2D * Cluster3 = new TH2D();
    Cluster3 = (TH2D*)file_dut3->Get(clust3_path.c_str()); 
    //Cluster respect reference plane 1
    TProfile2D *Tracks3 = new TProfile2D() ;
    Tracks3 = (TProfile2D*)file_dut3->Get(track3_path.c_str()) ;
    */
    /*************************************************************/

    /**************************************************************/
    //File for DUT4 with reference 1                                                                                                                    
    TFile *file_dut4 = new TFile(filename1.c_str());
    //Cluster respect reference plane 1                                                                                                                 
    TH2D * Cluster4 = new TH2D();
    Cluster4 = (TH2D*)file_dut4->Get(clust4_path.c_str());
    //Cluster respect reference plane 1                                                                                                                 
    TProfile2D *Tracks4 = new TProfile2D() ;
    Tracks4 = (TProfile2D*)file_dut4->Get(track4_path.c_str()) ;

    /*************************************************************/


    ofile << Tracks1->GetEntries() << " "
	  << Cluster1->GetEntries()<< " "
	  << Tracks2->GetEntries() << " "
	  << Cluster2->GetEntries()<< " "
      //<< Tracks3->GetEntries() << " "
      //<< Cluster3->GetEntries()<< " "
          << Tracks4->GetEntries() << " "
	  << Cluster4->GetEntries()<< " "
          << Events1->GetEntries() << " "
	  << endl ; 
  }

  
}
