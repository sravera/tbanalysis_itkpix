[plane1]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane1/mask_plane1.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -0.336555deg,0.427828deg,-0.267113deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 2.00505mm,84.898um,0um
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

[plane2]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane2/mask_plane2.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0.197785deg,-0.748856deg,-0.222823deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 1.77644mm,-218.012um,155mm
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

[plane3]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane3/mask_plane3.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -0.102101deg,0.90619deg,0.260639deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 1.86127mm,-496.129um,304mm
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

[plane130]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane130/mask_plane130.txt"
material_budget = 0.01
number_of_pixels = 400, 384
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 50um,50um
position = 380um,-2.5mm,442mm
role = "dut"
spatial_resolution = 14.434um,14.434um
time_resolution = 200ns
type = "rd53b"

[plane131]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane131/mask_plane131.txt"
material_budget = 0.01
number_of_pixels = 200, 768
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 100um,25um
position = 1.5mm,-3.7mm,481mm
role = "dut"
spatial_resolution = 29.8um,7.9um
time_resolution = 200ns
type = "rd53b"

[plane132]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane132/mask_plane132.txt"
material_budget = 0.01
number_of_pixels = 400, 384
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 50um,50um
position = 1.3mm,-4.3mm,537mm
role = "dut"
spatial_resolution = 14.434um,14.434um
time_resolution = 200ns
type = "rd53b"

[plane4]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane4/mask_plane4.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0um,0um,780mm
role = "reference"
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

[plane5]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane5/mask_plane5.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0.274905deg,-0.865625deg,-0.159798deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 555.512um,-376.936um,931mm
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

[plane6]
coordinates = "cartesian"
mask_file = "/afs/cern.ch/user/s/sravera/corry23/tbanalysis_rd53b/reconstruction/MaskCreator/plane6/mask_plane6.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0.017819deg,-0.55279deg,0.159397deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 852.618um,-289.598um,1035mm
spatial_resolution = 4.5um,4.5um
time_resolution = 230us
type = "mimosa26"

