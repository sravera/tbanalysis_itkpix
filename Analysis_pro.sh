##############################
# Usage:                      #
# source Analysis.sh          #
###############################
#!/bin/bash
HERE=$PWD

#path to the folder with the data
path_to_data="/eos/atlas/atlascerngroupdisk/det-itk/general/pixels/testbeam/cern_2023_august2nd_itk/raw/"
#run numbers to be analysed
run_numbers=(12300) 

#Reference palne for efficiency analysis
dut0="plane130"

#particle energy
momentum="120GeV"

for i in ${run_numbers[@]}; do
  data_file_name="${path_to_data}run0${i}.raw"
  #echo "----------- MASK WITH RUN ${i}$ -----------------"
  #corry -c 01_create_mask.conf -o EventLoaderEUDAQ.file_name=$data_file_name -o detectors_file="geometries/spsoriginal.geo" -o  histogram_file="mask_run00${i}.root" -o number_of_events=600000 -o Tracking4D.momentum=$momentum
  #echo "---Prealignment run ${i}$ ----"
  #corry -c 02_prealignment.conf -o EventLoaderEUDAQ.file_name=$data_file_name -o number_of_events=15000 -o detectors_file="geometries/start.geo" -o Tracking4D.momentum=$momentum
  #cp geometries/prealignment.geo  geometries/alignment_tel.geo
  #echo "--- Correlations run ${i}$ ----"
  #corry -c 03_correlations.conf -o EventLoaderEUDAQ.file_name=$data_file_name -o number_of_events=60000 -o Tracking4D.momentum=$momentum   
  #echo "---- Alignment tel 00 run ${i} ----"
  #corry -c 04_align_telescope.conf -o EventLoaderEUDAQ.file_name=$data_file_name  -o number_of_tracks=15000 -o detectors_file="geometries/prealignment.geo" -o Tracking4D.momentum=$momentum -o Tracking4D.spatial_cut_abs=100um,100um -o AlignmentTrackChi2.max_track_chi2ndof=15
  #echo "---- Alignment tel 01 run ${i} ----"
  #corry -c 04_align_telescope.conf -o EventLoaderEUDAQ.file_name=$data_file_name  -o number_of_tracks=15000 -o detectors_file="geometries/prealignment.geo" -o Tracking4D.momentum=$momentum -o Tracking4D.spatial_cut_abs=50um,50um -o AlignmentTrackChi2.max_track_chi2ndof=5  
  #echo "---- Alignment Tel run ${i} ----"
  #corry -c 04_align_telescope.conf -o EventLoaderEUDAQ.file_name=$data_file_name  -o number_of_tracks=5000 -o detectors_file="geometries/alignment_tel.geo" -o Tracking4D.momentum=$momentum -o Tracking4D.spatial_cut_abs=30um,30um -o AlignmentTrackChi2.max_track_chi2ndof=3

  #echo "---- Alignment DUT run ${i} ----" 
  #corry -c 05_TelTracks_alignDUTs.conf -o DUTAssociation.spatial_cut_rel=15 -o detectors_file="$HERE/geometries/alignment_tel.geo" -o EventLoaderEUDAQ.file_name=$data_file_name -o number_of_tracks=50000 -o Tracking4D.momentum=$momentum
  #corry -c 05_TelTracks_alignDUTs.conf -o DUTAssociation.spatial_cut_rel=10 -o EventLoaderEUDAQ.file_name=$data_file_name -o number_of_tracks=50000 -o Tracking4D.momentum=$momentum
  corry -c 05_TelTracks_alignDUTs.conf -o DUTAssociation.spatial_cut_rel=5 -o EventLoaderEUDAQ.file_name=$data_file_name -o number_of_tracks=5000 -o Tracking4D.momentum=$momentum
  corry -c 05_TelTracks_alignDUTs.conf -o DUTAssociation.spatial_cut_rel=3 -o EventLoaderEUDAQ.file_name=$data_file_name -o number_of_tracks=5000 -o Tracking4D.momentum=$momentum
  

  echo "---- HIT EFFICIENCY FOR RUN ${i} respect to SCC4 - RD53B ----"
  mkdir run${i} 
  #corry -c 06_analysis.conf -g ${dut0}.role="none" -o EventLoaderEUDAQ.file_name=$data_file_name -o detectors_file="geometries/alignment_final.geo" -o histogram_file="run0${i}.root" -o output_directory="run${i}" -o number_of_events=10000 -o Tracking4D.momentum=$momentum

  corry -c 06_analysis.conf -g ${dut0}.role="none" -o EventLoaderEUDAQ.file_name=$data_file_name -o detectors_file="geometries/alignment_final.geo" -o histogram_file="run0${i}.root" -o output_directory="run${i}" -o Tracking4D.momentum=$momentum 
  
  cp reconstruction/align_DUTs.root run${i}
  #cp reconstruction/align_telescope.root run${i}
  
  cp -r geometries/alignment_final.geo run${i}
  

  
done


